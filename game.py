import random
import player
import numpy as np

class Game():
	'''
	A self contained class for playing the Liars Dice game
	Attributes:
	Players 					: List, filled with players
	NumDice, (Number of Dice)	: Integer, the number of dice left in the game
	Current_Player 				: Integer, the location in the list of players 
	Turns 						: Integer, determine if it's the first turn in a round

	Methods:
	Turn:
	A player decides if they think the previous bid is false, if they choose it's not a lie then they propose their own bid

	Round:
	A round consists of Turns players bidding until one of the players believes the previous bid was false and then the bid is checked
	if the bid is false then the player making the bid loses a dice. If the bid is true then the player who called it false loses a dice
	'''
	
	def __init__(self,roster):
		dice_per_round = 6
		
		#fill the list of players in a random order 
		self.Players = roster
		
		#give each player 5 dice
		for player in xrange(len(self.Players)):
			self.Players[player].Dice = dice_per_round

		#set Game variables
		self.NumDice = len(roster) * dice_per_round
		self.Current_Player = 0
		self.Turns = 0
		self.Num_Players = len(roster)
		self.Bidding_Player = 0
		self.Opp_Dice = []
		self.History = []

	def Turn(self,cur_bid):
		self.Current_Player += 1
		self.History = []
		curp = self.Current_Player%self.Num_Players
		
		if self.Players[curp].Dice == 0:
			#if the player is out of dice skip them
			self.History.append('pass')
			return True, cur_bid

		else:
			#if the player has dice
			if self.turns == 0:
				#if it's the first turn of a new round
				
				decision, bid = self.Players[curp].player_choice(cur_bid, self.NumDice, self.History, self.Opp_Dice)
				self.History.append(bid)
				self.Bidding_Player = curp
				self.turns += 1

			else:
				#if it isn't a the first turn of a new round
				
				#move to the next player
				curp = self.Current_Player%self.Num_Players

				#determine that player's choice 
				decision, bid = self.Players[curp].player_choice(cur_bid, self.NumDice, self.History, self.Opp_Dice)
				self.History.append(bid)

				if decision == True:
					self.Bidding_Player = curp

				self.turns += 1

		return decision, bid

	def Round(self):
		#base variables for the round
		self.turns = 0 	#reset turn counter
		self.Opp_Dice = [self.Players[x].Dice for x in xrange(len(self.Players))]
		full_draw = [] 	#reset the dice counter
		decision = True	#reset this just to get into the while loop
		bid = [0,6]		#base bid, lowest possible bid
		players_out = 0 #reset out counter

		#draw a new hand for each player and add it into the round's card count
		for player in xrange(len(self.Players)):

			self.Players[player].New_Hand()
			full_draw += self.Players[player].Hand

			#check if the player is out
			if self.Players[player].Dice == 0:
				players_out +=1	
				

		#determine if there is more than one player left				
		if players_out == len(self.Players)-1:

			#find the player that still has dice
			winner = np.argmax(np.array([self.Players[i].Dice for i in xrange(len(self.Players))]))
			#print "winner", winner
			return winner

		
		"""
		After the round is initialized and we've checked that the game hasn't ended we play through the round
		turn by turn, until a player calls false
		"""

		while decision:
		#take turns until a player calls false
			decision, bid = self.Turn(bid)


		#check if the bid is accurate

		curp = self.Current_Player%self.Num_Players

		num_count = full_draw.count(bid[1]) + full_draw.count(1)
	#	print "there are " + str(num_count) + " " + str(bid[1]) + "'s"		
		
		
		if bid[0] < num_count:

			#the bid was true, the calling player loses a dice

			#print "Yar! It be true!"
			
			
			self.Players[curp].Dice -= 1

		else:

			#the bid was false, the bidding player loses a dice
			#print "Ye be a liar"
			
			#skip the players who already have no dice
		
			self.Players[self.Bidding_Player].Dice -= 1
			self.Current_Player = self.Bidding_Player

		#Show everyone's hand after the round
		#print [(self.Players[i].Name,self.Players[i].Hand) for i in xrange(len(self.Players))]

		#remove a dice from the total pool
		self.NumDice -= 1

		
		return None

class Trial():
	"""
	Attributes: 
		Players 				: list of players, players and location to allow for 
		Winner 					: Player object


	Methods:
		New_Game:
			Start a new game from the list of players
	"""

	def __init__(self, Num_Players):

		self.Players = []

		for i in xrange(Num_Players-1):
			self.Players.append(player.Test_AI(0, 'Test player ' + str(i+1), "Test", 0))

		self.Players.append(player.Player_AI(0, "Harold", "Good Ai", 0))
		#self.Players.append(player.Player_AI(0, "Shane", "Good Ai"))
		#self.Players.append(player.Player_AI(0, "Penelope", "Good Ai"))
		#self.Players.append(player.Player_AI(0, "Gustav", "Good Ai"))
		#self.Players.append(player.Player_AI(0, "Theodore", "Good Ai"))
		#self.Players.append(player.Human(0, "Matt", "Human"))
		

		self.Winner = None
		self.N = Num_Players

	def Play_Game(self):
		base_locs = range(self.N)
		keys = range(self.N)
		random.shuffle(keys)
	#	print keys
	
		roster = [self.Players[i] for i in keys] 

		for i in xrange(len(self.Players)):
			self.Players[i].Position = keys[i]

		winner_dict = dict((key, value) for (key,value) in zip(base_locs,keys))
	#	print winner_dict


		new_game = Game(roster)
		
		winner = None

		while winner is None:
			winner = new_game.Round()

	#	print winner, winner_dict[winner], self.Players[winner_dict[winner]].Name
		self.Players[winner_dict[winner]].Wins += 1

		#determine who has the most wins
		if self.Winner is None:
			self.Winner = self.Players[winner_dict[winner]]
		

		else:
			if self.Players[winner_dict[winner]].Wins > self.Winner.Wins:
				self.Winner = self.Players[winner_dict[winner]]
