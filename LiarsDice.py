import game


num_players = 6

trials = game.Trial(num_players)

trials.Play_Game()


for i in xrange(10000):
	trials.Play_Game()


print "Congrats " + trials.Winner.Name + " with " + str(trials.Winner.Wins) + " wins."

print [(trials.Players[i].Name, trials.Players[i].Wins) for i in xrange(len(trials.Players))]

